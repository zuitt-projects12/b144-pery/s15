console.log("Hello world");
let num1;
let num2;


function getSum(num1, num2){
	return num1 + num2;
}

function getDifference(num1, num2){
	return num1 - num2;
}

function getProduct(num1, num2){
	return num1 * num2;
}

function getQuotient(num1, num2){
	return num1 / num2;
}

function mainProg(){
	num1 = parseInt(prompt("Provide a number"));
	num2 = parseInt(prompt("Provide another number"));
	if(!isNaN(num1) && !isNaN(num2)){
		if(num1 + num2 < 10){
			console.warn(`The sum of the two numbers are: ${getSum(num1, num2)}.`);
		}
		else if(num1 + num2 >= 10 && num1 + num2 <= 20){
			alert(`The difference of the two numbers are: ${getDifference(num1, num2)}.`);	
		}
		else if(num1 + num2 >= 21 && num1 + num2 <= 30){
			alert(`The product of the two numbers are: ${getProduct(num1, num2)}.`);
		}
		else{
			alert(`The quotient of the two numbers are: ${getQuotient(num1, num2)}.`);
		}
	}
	else{
		alert("Please enter two numbers!");
		mainProg();
	}
}

mainProg();

function isLegalAge(age){
	if(age > 18){
		alert("You are of legal age.");
	}
	else{
		alert("You are not allowed here.");
	}
}

let name = prompt("Enter your name here");
let age = parseInt(prompt("Enter your age here"));
if(name === "" || isNaN(age)){
	alert("Are you a time traveler?");
}

else{
	alert(`Hello ${name}. Your age is ${age}.`);
	age = parseInt(age);
	isLegalAge(age);
	if (age >= 18){
		switch(age){
			case 18:
			case 19:
			case 20:
			case 21:
				alert("You are now allowed to party.");
					break;
			case 22:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
			case 30:
			case 31:
			case 32:
			case 33:
			case 34:
			case 35:
			case 36:
			case 37:
			case 38:
			case 39:
			case 40:
			case 41:
			case 42:
			case 43:
			case 44:
			case 45:
			case 46:
			case 47:
			case 48:
			case 49:
			case 50:
			case 51:
			case 52:
			case 53:
			case 54:
			case 55:
			case 56:
			case 57:
			case 58:
			case 59:
			case 60:
			case 61:
			case 62:
			case 63:
			case 64:
				alert("You are now part of the adult society.");
				break;
			case 65:
				alert("We thank you for your contribution to society");
			default:
				alert("Are you sure you're not an alien?");
		} /* ./switch */
	} /* ./if(age >= 18) */
} /*./ else*/

try{
	isLgealAge(age);
}
catch(error){
	console.warn(error);
}
finally{
	if(!isNaN(age)){
		isLegalAge(age);
	}
}